import random

def display_board(board):
    print(board[7]+'_|_'+board[8]+'_|_'+board[9])
    print(board[4]+'_|_'+board[5]+'_|_'+board[6])
    print(board[1]+' | '+board[2]+' | '+board[3])

def player_input():
    marker=''
    while marker != 'X' or marker !='O':
        marker=input('Player 1: X or O =').upper()

        if marker=='X':
            return('X','O')
        else:
            return('O','X')

def win_check(board,mark):
    return((board[7]==mark and board[8]==mark and board[9]==mark)or
           (board[4]==mark and board[5]==mark and board[6]==mark)or
           (board[1]==mark and board[2]==mark and board[3]==mark)or
           (board[1]==mark and board[4]==mark and board[7]==mark)or
           (board[2]==mark and board[5]==mark and board[8]==mark)or
           (board[3]==mark and board[6]==mark and board[9]==mark)or
           (board[1]==mark and board[5]==mark and board[9]==mark)or
           (board[3]==mark and board[5]==mark and board[7]==mark))

def choose_first():
    t=random.randint(0,1)
    if t==0:
        return 'Player 1'
    else:
        return 'Player 2'

def space_check(board,position):
        return board[position]==' '

def full_board_check(board):
    for i in range(1,10):
        if space_check(board,i):
            return False
    return True

def player_choice(board,turn):
    if turn=='Player 1':
        position=0
        while position not in [1,2,3,4,5,6,7,8,9] or not space_check(board,position):
            position=int(input('Choose posiiton(1-9)='))
        return position
    else:
        position = 0
        while position not in [1,2,3,4,5,6,7,8,9] or not space_check(board, position):
            position = random.randint(1,10)
        return position

print('Lets play TIC TAC TOE')
game_no=1
d={}
l='yes'
while l=='yes':
    list=[]
    the_board=[' ']*10
    print("\nGame {}".format(game_no))
    player1_marker, player2_marker = player_input()
    turn = choose_first()
    print(turn + ' will go first')
    while not full_board_check(the_board):
        if turn=='Player 1':
            print("Your turn")
            display_board(the_board)
            position=player_choice(the_board,turn)
            the_board[position]=player1_marker
            if win_check(the_board,player1_marker):
                print("\n")
                display_board(the_board)
                print('You win!')
                status='You won'
                break
            else:
                if full_board_check(the_board):
                    print("\n")
                    display_board(the_board)
                    print("Tie Game!")
                    status='Tie'
                    break
                else:
                    turn='Player 2'
        else:
            print("Computer's turn")
            display_board(the_board)
            position = player_choice(the_board, turn)
            the_board[position]=player2_marker
            if win_check(the_board, player2_marker):
                print("\n")
                display_board(the_board)
                print('You lose!')
                status='Computer won'
                break
            else:
                if full_board_check(the_board):
                    print("\n")
                    display_board(the_board)
                    print("Tie Game!")
                    status='Tie'
                    break
                else:
                    turn = 'Player 1'
    list=[the_board,status]
    d[game_no]=list
    game_no=game_no+1
    l=input("\nPlay again? yes/no=")

ch=input("\nWant to check history? yes/no=")
while(ch=='yes'):
    game= int(input("Enter game number="))
    print("Stats")
    b=d[game][0]
    s=d[game][1]
    display_board(b)
    print(s)
    ch=input("\nDo you want to continue checking? yes/no=")





